'use strict'

//Wrapper function which encapsulates grunt configuration
module.exports = function(grunt) {

	// Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    // Automatically load required grunt tasks
    require('jit-grunt')(grunt, {
    	//To inform jit that useminPrepare task depends
        //on usemin package
    	useminPrepare: 'grunt-usemin'
    });

    //Define configuration for all the tasks
    grunt.initConfig({

    	// Converting scss to css
    	sass: {
    		dist: {
    			files: {
    				'css/styles.css':'css/styles.scss'
    			}
    		}
    	},

    	watch: {
    		files: 'css/*.scss',
    		tasks: ['sass']
    	},

    	browserSync: {
    		dev: {
    			bsFiles: {
    				src: [
    					'css/*.css',
    					'*.html',
    					'js/*.js'
    				]
    			},

    			options: {
    				watchTask: true,
    				server: {
    					baseDir: './' //Current directory
    				}
    			}
    		}
    	},

    	//Copying files to distribution folder
    	copy: {
    		html: {
    			files: [{
                    //for html
    				expand: true,
    				dot: true,
    				cwd: './',
    				src: ['*.html'],
    				dest: 'dist'
    			}]
    		},

    		fonts: {
    			files: [{
                    //for font-awesome
    				expand: true,
    				dot: true,
    				cwd: 'node_modules/font-awesome',
    				src: ['fonts/*.*'],
    				dest: 'dist'
    			}]
    		}
    	},

    	clean: {
    		build: {
    			src: ['dist/']
    		}
    	},

    	imagemin: {
    		dynamic: {
    			files: [{
    				expand: true, //Enable dynamic expansion
    				cwd: './', //Src matches are relative to this path
    				src: ['img/*.{png,gif,jpg}'], //Actual patterns to match
    				dest: 'dist'  //Destination path prefix
    			}]
    		}
    	},

    	useminPrepare: {
    		foo: {
    			dest: 'dist',
    			src: ['contactus.html','aboutus.html','index.html']
    		},

    		options: {
    			flow: {
    				steps: {
    					css: ['cssmin'],
    					js: ['uglify']
    				},

    				post: {
    					css: [{
    						name: 'cssmin',
    						createConfig: function(context,block){
    							var generated = context.options.generated;
    							generated.options = {
    								keepSpecialComments: 0,
    								rebase: false
    							};
    						}
    					}]
    				}
    			}
    		}
    	},

        //Concat
    	concat: {
    		options: {
                //Defines a string to put between each file in the concatenated output
    			separator: ';'
    		},

            //dist configuration is provided by ueminPrepare
    		dist: {}
    	},

    	uglify: {
    		dist: {}
    	},

    	cssmin: {
    		dist: {}
    	},

    	filerev: {
    		options: {
    			encoding: 'utf8',
    			algorithm: 'md5',
    			length: '20'
    		},

    		release: {
                //filerev:release hashes(md5) all assets (images, js and css) in dist directory
    			files: [{
    				src: ['dist/css/*.css', 'dist/js/*.js']
    			}]
    		}
    	},

        // Usemin
        // Replaces all assets with their revved version in html and css files.
        // options.assetDirs contains the directories for finding the assets
        // according to their relative paths
    	usemin: {
    		html: ['dist/contactus.html', 'dist/aboutus.html', 'dist/index.html'],
    		options: {
    			assetsDir: ['dist', 'dist/css', 'dist/js']
    		}
    	}

    });

    grunt.registerTask('css',['sass']);
    grunt.registerTask('default', ['browserSync', 'watch']);  //Always browserSync first and then watch
    grunt.registerTask('build', [
    	'clean',
    	'copy',
    	'imagemin',
    	'useminPrepare',
    	'concat',
    	'cssmin',
    	'uglify',
    	'filerev',
    	'usemin'
    ]);

}