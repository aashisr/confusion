'use strict';

// Include all gulp plugins
var gulp = require('gulp'),
	sass = require('gulp-sass'),
	browserSync = require('browser-sync'),
	del = require('del'),
	imagemin = require('gulp-imagemin'),
	uglify = require('gulp-uglify'),
	usemin = require('gulp-usemin'),
	rev = require('gulp-rev'),
	cleanCss = require('gulp-clean-css'),
	flatmap = require('gulp-flatmap'),
	htmlmin = require('gulp-htmlmin');

//Adding gulp tasks for Sass and Browser-Sync
gulp.task('sass', function(){
	return gulp.src('./css/*.sass')
	.pipe(sass().on('error',sass.logError))
	.pipe(gulp.dest('./css'));
});

//Watch files
gulp.task('sass:watch', function(){
	gulp.watch('./')
});

//Browser Sync task
gulp.task('browser-sync', function(){
	var files = [
		'./*.html',
		'./css/*.css',
		'./img/*.{png,gif,jpg}',
		'./js/*.js'
	];  

	//Initialize browserSync
	browserSync.init(files, {
		server: {
			baseDir: './'
		}
	});
});

//Configure default task
//browser-sync task should run before running sass:watch
gulp.task('default',['browser-sync'], function(){
	gulp.start('sass:watch');
});

//Clean task
gulp.task('clean', function(){
	return del(['dist']);
});

//Copy task .. No specific module needed
gulp.task('copyfonts', function(){
	gulp.src('./node_modules/font-awesome/fonts/**/*.{ttf, woff, eof, svg}*')
	.pipe(gulp.dest('./dist/fonts'));
});

//Imagemin task
gulp.task('imagemin', function(){
	return gulp.src('img/*.{png,jpg,gif}')
	.pipe(imagemin({optimizationLevel: 3, progressive: true, interlaced: true}))
	.pipe(gulp.dest('dist/img'));
});

//Usemin task
//Flatmap maps each file in a stream into multiple files
//that are piped out
gulp.task('usemin', function(){
	return gulp.src('./*.html')
	.pipe(flatmap(function(stream, file){
		return stream
		.pipe(usemin({
			css: [rev()],
			html: [function() { return htmlmin({collapseWhitespace: true})}],
			js: [uglify(), rev()],
			inlinejs: [uglify()],
			inlinecss: [cleanCss(), 'concat']
		}))
	}))
	.pipe(gulp.dest('dist/'))
});

//Build task ,, clean must be executed before build
gulp.task('build',['clean'], function(){
	gulp.start('copyfonts', 'imagemin', 'usemin');
});

